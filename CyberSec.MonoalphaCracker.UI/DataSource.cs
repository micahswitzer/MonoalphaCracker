﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CyberSec.MonoalphaCracker.UI
{
    public class DataSource : INotifyPropertyChanged
    {
        public Dictionary<char, TextBox> LetterTextBoxes { get; private set; }
        public Dictionary<char, char> LetterMappings { get; private set; }
        public Dictionary<char, double> CipherLetterFreq { get; private set; }
        public Dictionary<char, double> PlainLetterFreq { get; private set; }

        private bool _highlightEnabled = true;
        public bool HighlightEnabled
        {
            get => _highlightEnabled;
            set => NotifyPropertyChanged(ref _highlightEnabled, value);
        }

        private string _ciphertext;
        public string Ciphertext
        {
            get => _ciphertext;
            set => NotifyPropertyChanged(ref _ciphertext, value);
        }

        public DataSource()
        {
            LetterTextBoxes = new Dictionary<char, TextBox>();
            LetterMappings = new Dictionary<char, char>();
            CipherLetterFreq = new Dictionary<char, double>();
            PlainLetterFreq = new Dictionary<char, double>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged<T>(ref T prop, T val, [CallerMemberName] string propName = "")
        {
            if (prop?.Equals(val) ?? false) return;
            prop = val;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
