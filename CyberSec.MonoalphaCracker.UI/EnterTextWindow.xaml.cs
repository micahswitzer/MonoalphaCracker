﻿using System.Windows;
using System.IO;
using Microsoft.Win32;

namespace CyberSec.MonoalphaCracker.UI
{
    /// <summary>
    /// Interaction logic for EnterTextWindow.xaml
    /// </summary>
    public partial class EnterTextWindow : Window
    {
        public new string Content
        {
            get => textInput.Text;
            set => textInput.Text = value;
        }

        public EnterTextWindow()
        {
            InitializeComponent();
        }

        private void Default_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Load_OnClick(object sender, RoutedEventArgs e)
        {
            // implement load from file
            var dialog = new OpenFileDialog { Filter = "Text Files | *.txt" };
            if (dialog.ShowDialog().GetValueOrDefault())
            {
                var path = dialog.FileName;
                try
                {
                    var data = File.ReadAllText(path);
                    textInput.Text = data.ToUpper();
                }
                catch (IOException)
                {
                    MessageBox.Show("Error loading file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
