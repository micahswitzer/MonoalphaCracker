﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace CyberSec.MonoalphaCracker.UI
{
    /// <summary>
    /// Interaction logic for ReplaceLetterWindow.xaml
    /// </summary>
    public partial class ReplaceLetterWindow : Window
    {
        private readonly char letter;

        public ReplaceLetterWindow(char letter)
        {
            this.letter = letter;
            lastValid = letter.ToString();
            InitializeComponent();
        }

        private string lastValid;

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            replaceLabel.Content = $"Replace {letter} with:";

            letterBox.GotFocus += (sender, args) => ((TextBox)sender).Focus();
            letterBox.TextChanged += (sender, args) =>
            {
                var textBox = (TextBox)sender;
                var validData = textBox.Text.Length == 1;
                if (validData)
                {
                    var newLetter = textBox.Text.ToUpper().First();
                    if (newLetter >= 'A' && newLetter <= 'Z')
                    {
                        lastValid = newLetter.ToString();
                    }
                    else validData = false;
                }
                textBox.Text = lastValid;
                textBox.SelectAll();
            };
            cancelButton.Click += (sender, args) =>
            {
                DialogResult = false;
                Close();
            };
            okButton.Click += (sender, args) =>
            {
                DialogResult = true;
                Close();
            };

            letterBox.Focus();
            letterBox.SelectAll();
        }

        public new string Content
        {
            get => letterBox.Text;
            set => letterBox.Text = value;
        }
    }
}
