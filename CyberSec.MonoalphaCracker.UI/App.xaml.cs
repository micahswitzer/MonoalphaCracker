﻿using System;
using System.Windows;

namespace CyberSec.MonoalphaCracker.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Lazy<DataSource> _dSource = new Lazy<DataSource>();
        public DataSource DataSource => _dSource.Value;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MainWindow = new MainWindow(DataSource);
            MainWindow.Show();
        }
    }
}
