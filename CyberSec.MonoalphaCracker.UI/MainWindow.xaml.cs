﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CyberSec.MonoalphaCracker.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataSource _ds;
        private bool _lastValid = true;
        private MultiValueDictionary<char, Label> _charLabels = new MultiValueDictionary<char, Label>();

        private Brush _foreHighlightBrush = new SolidColorBrush(Color.FromRgb(0, 127, 0));
        private Brush _foreNormalBrush    = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        private Brush _backHighlightBrush = new SolidColorBrush(Color.FromRgb(255, 255, 0));
        private Brush _backNormalBrush    = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));

        public MainWindow(DataSource dataSource)
        {
            _ds = dataSource;
            DataContext = dataSource;
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            for (char c = 'A'; c <= 'Z'; c++)
            {
                var stackPanel = new StackPanel();
                var label = new Label { Content = c };
                var textBox = new TextBox { Text = c.ToString(), Tag = c };
                textBox.GotFocus += TextBox_GotFocus;
                textBox.TextChanged += TextBox_TextChanged;
                _ds.LetterTextBoxes.Add(c, textBox);
                _ds.LetterMappings.Add(c, c);
                stackPanel.Children.Add(label);
                stackPanel.Children.Add(textBox);
                letterSelectorPanel.Children.Add(stackPanel);
            }
            _ds.LetterTextBoxes['A'].Focus();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            textBox.SelectAll();
        }

        private void TextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            var letter = (char)textBox.Tag;
            var validData = textBox.Text.Length == 1;
            if (validData)
            {
                var newLetter = textBox.Text.ToUpper().First();
                if (newLetter == _ds.LetterMappings[letter] && !_lastValid) return;
                if (newLetter >= 'A' && newLetter <= 'Z')
                {
                    _ds.LetterMappings[letter] = newLetter;
                    SubCiphertext(letter, false);
                }
                else validData = false;
            }
            _lastValid = validData;
            textBox.Text = _ds.LetterMappings[letter].ToString();
            if (validData)
                _ds.LetterTextBoxes[++letter <= 'Z' ? letter : 'A'].Focus();
            else
                textBox.SelectAll();
        }

        private void InitCiphertext()
        {
            var lines = _ds.Ciphertext.Split(new[] { "\r\n" }, StringSplitOptions.None);
            _ds.HighlightEnabled = _ds.Ciphertext.Length < 5500;
            var ff = new FontFamily("Consolas");
            var padding = new Thickness(0);
            ciphertextPanel.Children.Clear();
            _charLabels.Clear();
            foreach (var line in lines)
            {
                var stackPanel = new StackPanel { Orientation = Orientation.Horizontal };
                foreach (var c in line)
                {
                    bool isSubChar = c >= 'A' && c <= 'Z';
                    char cipherChar = isSubChar ? _ds.LetterMappings[c] : c;
                    Brush brush = cipherChar != c ? _foreHighlightBrush : _foreNormalBrush;
                    var label = new Label { Content = cipherChar, Tag = c, FontFamily = ff, Padding = padding, Foreground = brush };
                    stackPanel.Children.Add(label);
                    if (c < 'A' || c > 'Z') continue;
                    label.MouseEnter += Label_MouseEnter;
                    label.MouseLeave += Label_MouseLeave;
                    label.MouseDoubleClick += Label_DoubleClick;
                    _charLabels.Add(c, label);
                }
                ciphertextPanel.Children.Add(stackPanel);
            }
        }

        private void Label_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            var label = (Label)sender;
            var letter = (char)label.Tag;
            var dialog = new ReplaceLetterWindow(letter) { Content = _ds.LetterMappings[letter].ToString() };
            if (!dialog.ShowDialog().GetValueOrDefault()) return;
            var newLetter = dialog.Content.First();
            _ds.LetterMappings[letter] = newLetter;
            SubCiphertext(letter);
        }

        private void Label_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!_ds.HighlightEnabled) return;
            var label = (Label)sender;
            var letter = (char)label.Tag;
            foreach (var l in _charLabels[letter])
            {
                l.Background = _backNormalBrush;
            }
        }

        private void Label_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!_ds.HighlightEnabled) return;
            var label = (Label)sender;
            var letter = (char)label.Tag;
            foreach (var l in _charLabels[letter])
            {
                l.Background = _backHighlightBrush;
            }
        }

        private void SubCiphertext(char c, bool updateTextbox = true)
        {
            string str = _ds.LetterMappings[c].ToString();
            if (updateTextbox) _ds.LetterTextBoxes[c].Text = str;
            if (!_charLabels.ContainsKey(c)) return;
            Brush brush = _ds.LetterMappings[c] != c ? _foreHighlightBrush : _foreNormalBrush;
            foreach (var label in _charLabels[c])
            {
                label.Content = str;
                label.Foreground = brush;
            }
        }

        private void LoadCiphertext()
        {
            var modal = new EnterTextWindow { Title = "Enter Ciphertext", Content = _ds.Ciphertext };
            var res = modal.ShowDialog().GetValueOrDefault();
            if (res)
            {
                _ds.Ciphertext = modal.Content.ToUpper();
                ComputeLetterFreq(_ds.Ciphertext, _ds.CipherLetterFreq);
                AutoSolve(); // attempt an auto-solve
                InitCiphertext();
            }
        }

        private void ComputeLetterFreq(string sample, Dictionary<char, double> dictionary)
        {
            var tempDic = new Dictionary<char, int>();
            var total = 0;
            for (char c = 'A'; c <= 'Z'; c++)
            {
                tempDic.Add(c, 0);
            }
            foreach (var c in sample)
            {
                if (c < 'A' || c > 'Z') continue;
                tempDic[c]++;
                total++;
            }
            dictionary.Clear();
            var newTot = (double)total;
            foreach (var pair in tempDic.OrderByDescending(x => x.Value))
            {
                dictionary.Add(pair.Key, pair.Value / newTot);
            }
        }

        private void EditCiphertext_Click(object sender, RoutedEventArgs e)
        {
            LoadCiphertext();
        }

        private void LoadFrequencySample_Click(object sender, RoutedEventArgs e)
        {
            var modal = new EnterTextWindow { Title = "Enter Plaintext" };
            var res = modal.ShowDialog().GetValueOrDefault();
            if (res)
            {
                var sample = modal.Content.ToUpper();
                ComputeLetterFreq(sample, _ds.PlainLetterFreq);
                AutoSolve();
                MessageBox.Show("Letter frequency analysis complete");
            }
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            if ((_ds.Ciphertext?.Length).GetValueOrDefault() <= 0)
            {
                MessageBox.Show("No text to export!", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var sb = new StringBuilder(_ds.Ciphertext.Length);
            foreach (var ch in _ds.Ciphertext)
            {
                if (_ds.LetterMappings.ContainsKey(ch))
                    sb.Append((char)(_ds.LetterMappings[ch] - 'A' + 'a'));
                else
                    sb.Append(ch);
            }
            Clipboard.SetText(sb.ToString());
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AutoSolve()
        {
            if (_ds.PlainLetterFreq.Count == 0 || _ds.CipherLetterFreq.Count == 0) return;
            for (int i = 0; i < 26; i++)
            {
                var plainChar = _ds.PlainLetterFreq.ElementAt(i).Key;
                var cipherChar = _ds.CipherLetterFreq.ElementAt(i).Key;
                _ds.LetterMappings[cipherChar] = plainChar;
                SubCiphertext(cipherChar);
            }
        }
    }
}
